{
  $jsonSchema: {
    bsonType: 'object',
    required: [
      '_id',
      'DataInicioVigencia',
      'DataReferencia',
      'DataFimVigencia',
      'CodigoOperacionalCorretor'
    ],
    additionalProperties: false,
    properties: {
      _id: {},
      DataInicioVigencia: {},
      DataReferencia: {},
      DataFimVigencia: {},
      EmailKitVirtual: {},
      IndicadorKitVirtual: {},
      NomeOperador: {},
      NumeroPropostaCorretor: {},
      IndicadorUtilizaTabelaPrazoCurto: {},
      IndicadorDevolverParaRenovacaoAutomatica: {},
      DataLimiteCalculoEnvioProposta: {},
      CodigoOperacionalCorretor: {},
      DataManutencao: {},
      Segurado: {
        bsonType: 'object',
        properties: {
          Nome: {},
          NumeroCpfCnpj: {}
        }
      },
      SituacaoAtual: {
        bsonType: 'object',
        properties: {
          Codigo: {},
          DataManutencao: {},
          Descricao: {},
          Usuario: {}
        }
      },
      SubEstipulante: {
        bsonType: 'object',
        properties: {
          CodigoCnae: {},
          CodigoOcupacao: {},
          DataNascimento: {},
          Email: {},
          EstadoCivil: {},
          Nome: {},
          NumeroCpfCnpj: {},
          Sexo: {},
          TipoPessoa: {}
        }
      },
      TipoCotacao: {
        bsonType: 'object',
        properties: {
          Codigo: {},
          Descricao: {}
        }
      },
      TipoRoteiro: {
        bsonType: 'object',
        properties: {
          Codigo: {},
          Descricao: {}
        }
      },
      FatorRandomico: {
        bsonType: 'object',
        properties: {
          Numero: {},
          NumeroRoteiro: {},
          Valor: {}
        }
      },
      NumeroApolice: {},
      DataCriacaoApolice: {},
      DataEnvioCartaRecusa: {},
      Corretores: {
        bsonType: [
          'array'
        ],
        items: {
          bsonType: [
            'object'
          ],
          additionalProperties: false,
          properties: {
            TipoCorretor: {
              bsonType: 'object',
              properties: {
                Codigo: {},
                Descricao: {}
              }
            },
            Nome: {},
            CodigoCliente: {},
            CodigoClienteOperacional: {},
            CodigoEstabelecimento: {},
            CodigoSusep: {},
            PercentualParticipacao: {},
            Endereco: {
              bsonType: 'object',
              properties: {
                ComplementoEndereco: {},
                NomeBairro: {},
                NomeCidade: {},
                NomeLogradouro: {},
                NumeroCep: {},
                NumeroLogradouro: {},
                SiglaUf: {},
                TipoLogradouro: {}
              }
            },
            TelefonesCorretor: {
              bsonType: [
                'array'
              ],
              items: {
                bsonType: [
                  'object'
                ],
                additionalProperties: false,
                properties: {
                  DescricaoTipo: {},
                  NumeroDdd: {},
                  Tipo: {},
                  RamalTelefone: {},
                  NumeroTelefone: {}
                }
              }
            }
          }
        }
      },
      FormasPagamento: {
        bsonType: [
          'array'
        ],
        items: {
          bsonType: [
            'object'
          ],
          required: [
            'Tipo',
            'NumeroParcelas',
            'IndicadorJuros'
          ],
          additionalProperties: false,
          properties: {
            Tipo: {},
            DiaPreferencialPagamento: {},
            NumeroParcelas: {},
            IndicadorJuros: {},
            QuantidadeCalculo: {},
            IndicadorFormaPagamentoSelecionado: {},
            ValorParcela: {},
            Descricao: {},
            ContaCorrente: {
              bsonType: 'object',
              properties: {
                AnoValidadeCartao: {},
                CodigoAgencia: {},
                CodigoBandeiraCartao: {},
                CodigoIdentificacaoCriptografia: {},
                DigitoAgencia: {},
                DigitoConta: {},
                IndicadorTitularConta: {},
                MesValidadeCartao: {},
                NomeBandeiraCartao: {},
                NomePortadorCartao: {},
                NomeTitular: {},
                NumeroBanco: {},
                NumeroConta: {},
                NumeroCpfCnpjTitular: {},
                NumeroQuatroUltimosDigitosCartao: {},
                TipoPessoa: {}
              }
            },
            CodigoFormaPagamento: {},
            ValorIof: {},
            ValorFracionado: {}
          }
        }
      },
      SeguradoEnderecos: {
        bsonType: [
          'array'
        ],
        items: {
          bsonType: [
            'object'
          ],
          additionalProperties: false,
          properties: {
            TipoEndereco: {
              bsonType: 'object',
              properties: {
                Codigo: {},
                Descricao: {}
              }
            },
            NumeroCep: {},
            TipoLogradouro: {},
            NomeLogradouro: {},
            NumeroLogradouro: {},
            ComplementoEndereco: {},
            NomeBairro: {},
            NomeCidade: {},
            SiglaUf: {}
          }
        }
      },
      StatusCotacao: {
        bsonType: [
          'array'
        ],
        items: {
          bsonType: [
            'object'
          ],
          additionalProperties: false,
          properties: {
            Codigo: {},
            Descricao: {},
            DataInclusao: {},
            Usuario: {}
          }
        }
      },
      TelefoneSegurado: {
        bsonType: [
          'array'
        ],
        items: {
          bsonType: [
            'object'
          ],
          additionalProperties: false,
          properties: {
            CodigoTipo: {},
            DescricaoTipo: {},
            NumeroDdd: {},
            NumeroTelefone: {},
            RamalTelefone: {}
          }
        }
      },
      Cenarios: {
        bsonType: [
          'array'
        ],
        items: {
          bsonType: [
            'object'
          ],
          required: [
            'IndicadorCenarioEscolhido',
            'IndicadorDescontoSeguroAuto',
            'TipoAssistencia'
          ],
          additionalProperties: false,
          properties: {
            IndicadorCenarioEscolhido: {},
            IndicadorDescontoSeguroAuto: {},
            TipoAssistencia: {},
            ValorPremioLiquido: {},
            ValorCustoAdicionalApolice: {},
            ValorDescontoPromocional: {},
            ValorCustoApolice: {},
            PercentualReconquista: {},
            PercentualProLaboreEstipulante: {},
            PercentualComissao: {},
            PercentualCoeficienteAgravo: {},
            PercentualProLaboreSubEstipulante: {},
            QuantidadeCalculo: {},
            PercentualIOF: {},
            ValorAdicionalFracionamento: {},
            TipoDescontoAgravo: {
              bsonType: 'object',
              properties: {
                Descricao: {},
                Percentual: {}
              }
            },
            Itens: {
              bsonType: [
                'array'
              ],
              items: {
                bsonType: [
                  'object'
                ],
                required: [
                  'CodigoCanal',
                  'CodigoProduto'
                ],
                additionalProperties: false,
                properties: {
                  CodigoCanal: {},
                  CodigoProduto: {},
                  ValorLimiteMaximoGarantia: {},
                  Renovacao: {
                    bsonType: 'object',
                    properties: {
                      CodigoSusepSeguradora: {},
                      FatorLimitador: {},
                      FatorOtimizado: {},
                      NomeSeguradora: {},
                      NumeroApolice: {},
                      NumeroItemApolice: {},
                      PercentualDescontoPromocao: {},
                      PeriodoCoberturaAnterior: {},
                      SinistroPremio: {},
                      ValorDesconto: {},
                      ValorDescontoFidelidade: {},
                      ValorDescontoPromocionalInformadoFilial: {}
                    }
                  },
                  EnderecoRisco: {
                    bsonType: 'object',
                    properties: {
                      CodigoTipoConstrucao: {},
                      CodigoTipoHomeOffice: {},
                      CodigoTipoResidencia: {},
                      CodigoTipoUtilizacao: {},
                      ComplementoEndereco: {},
                      DescricaoTipoConstrucao: {},
                      DescricaoTipoHomeOffice: {},
                      DescricaoTipoResidencia: {},
                      DescricaoTipoUtilizacao: {},
                      IndicadorImovelDesocupadoReformaConstrucao: {},
                      IndicadorImovelTombadoPatrimonioHistorico: {},
                      IndicadorMadeiraParedeExterna: {},
                      IndicadorRuralUrbano: {},
                      IndicadorSeguradoProprietario: {},
                      NomeBairro: {},
                      NomeCidade: {},
                      NomeLogradouro: {},
                      NumeroCep: {},
                      NumeroLogradouro: {},
                      SiglaUf: {},
                      TipoLogradouro: {}
                    }
                  },
                  DescontoProtecaoRoubo: {
                    bsonType: 'object',
                    properties: {
                      IndicadorAlarme: {},
                      IndicadorCercaEletrica: {},
                      IndicadorGradesProtecao: {},
                      PercentualDesconto: {}
                    }
                  },
                  Coberturas: {
                    bsonType: [
                      'array'
                    ],
                    items: {
                      bsonType: [
                        'object'
                      ],
                      required: [
                        'Codigo',
                        'Nome',
                        'ValorLMI'
                      ],
                      additionalProperties: false,
                      properties: {
                        Codigo: {},
                        Nome: {},
                        TipoCoberturaIncendio: {},
                        ValorLMI: {},
                        ParticipacaoObrigatoria: {},
                        CodigoVerba: {},
                        CodigoFranquia: {},
                        NomeVerba: {},
                        NomeFranquia: {}
                      }
                    }
                  },
                  Beneficiarios: {
                    bsonType: [
                      'array'
                    ],
                    items: {
                      bsonType: [
                        'object'
                      ],
                      additionalProperties: false,
                      properties: {
                        Nome: {}
                      }
                    }
                  },
                  Contatos: {
                    bsonType: [
                      'array'
                    ],
                    items: {
                      bsonType: [
                        'object'
                      ],
                      additionalProperties: false,
                      properties: {
                        Telefone: {},
                        Nome: {}
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
}