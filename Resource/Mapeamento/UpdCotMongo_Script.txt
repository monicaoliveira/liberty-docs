====================== UPD 628 - ACEITE
db.CotacaoResidencia.update(
   { _id: 628},
   {
     $set: {
        "DataInicioVigencia" : ISODate("2019-11-16T03:00:00.000Z"),
        "DataFimVigencia" : ISODate("2020-11-16T03:00:00.000Z"),
     }
   }
)

====================== UPD 651 - ACEITE
db.CotacaoResidencia.update(
   { _id: 651},
   {
     $set: {
		"DataLimiteCalculoEnvioProposta" : ISODate("2019-11-29T13:03:55.474Z")
     }
   }
)

====================== UPD 419 - DESENVOLVIMENTO
db.CotacaoResidencia.update(
   { _id: 419},
   {
     $set: {
		"DataLimiteCalculoEnvioProposta" : ISODate("2019-11-29T13:03:55.474Z")
     }
   }
)