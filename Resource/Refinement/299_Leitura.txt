Lendo história 299
Adaptar fluxos pós aprovação (inspeção)


*** PLANILHA INPESÇÃO DE RISCO ***
Contem o agrupamento de campos para aplicar a regra de INSPEÇÃO:

AGRUPAMENTO
===========
1.PRODUTO ( 11030 RESIDENCIA )
2.COBERTURA ( 114141 ROUBO E OU FURTO QUAL. DE BENS VERANEIO )
3.VERBA ( R04 VERBA UNICA )
4.AGRUPAMENTO ( 4940 )
5.TIPO RESIDENCIA ( CASA EM CONDOMINIO FECHADO )
6.TIPO UTILIZAÇÃO ( VERANEIO ) 
7.TIPO CONSTRUÇÃO ( MADEIRA - MISTA INFERIOR )


CRITERIO
========
Valor do LMI entre   R$20.000,01  e  R$1.000.000.000,00 


GERAR OCORRÊNCIA DE PROPOSTA COM RESTRIÇÃO
==========================================
Restrição
INSPEÇÃO DE RISCO

Ocorrência
LMI SUPERIOR

Texto
As condições desta cotação e a aceitação do risco dependerão de prévia análise da matriz, mediante inspeção de risco.



VALIDAR CAMPOS OBRIGATÓRIOS
============================
O nome do contato responsável pela inspeção de risco deve ser informado. (Item 1)
O número do telefone do contato responsável pela inspeção de risco deve ser informado. (Item 1)
