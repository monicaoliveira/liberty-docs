/* 1 */
{
    "_id" : 432,
    "DataInicioVigencia" : ISODate("2019-11-21T03:00:00.000Z"),
    "DataReferencia" : ISODate("2019-11-22T03:00:00.000Z"),
    "DataFimVigencia" : ISODate("2020-11-21T03:00:00.000Z"),
    "NumeroPropostaCorretor" : 0,
    "IndicadorUtilizaTabelaPrazoCurto" : false,
    "IndicadorDevolverParaRenovacaoAutomatica" : false,
    "CodigoOperacionalCorretor" : 99010532,
    "DataManutencao" : ISODate("2019-11-22T13:45:18.781Z"),
    "Segurado" : {
        "Nome" : "Cenario OcorrenciaCotacaoResidencia",
        "NumeroCpfCnpj" : NumberLong(22755252707)
    },
    "SituacaoAtual" : {
        "Codigo" : 1,
        "Descricao" : "Em cotação",
        "DataManutencao" : Date(-62135596800000),
        "Usuario" : null
    },
    "SubEstipulante" : {
        "CodigoCnae" : 0,
        "CodigoOcupacao" : 0,
        "DataNascimento" : null,
        "Email" : null,
        "EstadoCivil" : null,
        "Nome" : null,
        "NumeroCpfCnpj" : 0,
        "Sexo" : null,
        "TipoPessoa" : null
    },
    "TipoCotacao" : {
        "Codigo" : 1,
        "Descricao" : "Seguro Novo"
    },
    "TipoRoteiro" : {
        "Codigo" : 1,
        "Descricao" : "Tarifa Roteiro A"
    },
    "FatorRandomico" : {
        "Numero" : 1,
        "NumeroRoteiro" : 1,
        "Valor" : 1
    },
    "Corretores" : [ 
        {
            "TipoCorretor" : {
                "Codigo" : 1,
                "Descricao" : null
            },
            "Nome" : "MAPA ADMR E CORRETORA DE SEGUROS SC LTDA",
            "CodigoCliente" : 99005736,
            "CodigoClienteOperacional" : 99010532,
            "CodigoEstabelecimento" : 3,
            "CodigoSusep" : 0,
            "PercentualParticipacao" : 100,
            "Endereco" : {
                "NomeBairro" : "C MONCOES",
                "NomeCidade" : "SAO PAULO",
                "NomeLogradouro" : "DOUTOR GERALDO CAMPOS MOREIRA",
                "NumeroCep" : 4571020,
                "NumeroLogradouro" : "100",
                "TipoLogradouro" : "RUA",
                "SiglaUf" : "SP",
                "ComplementoEndereco" : ""
            },
            "TelefonesCorretor" : [ 
                {
                    "DescricaoTipo" : "",
                    "NumeroDdd" : 11,
                    "RamalTelefone" : 0,
                    "NumeroTelefone" : 47374822,
                    "Tipo" : "1"
                }, 
                {
                    "DescricaoTipo" : "",
                    "NumeroDdd" : 0,
                    "RamalTelefone" : 0,
                    "NumeroTelefone" : 0,
                    "Tipo" : "2"
                }
            ]
        }
    ],
    "FormasPagamento" : [ 
        {
            "Tipo" : "FB",
            "NumeroParcelas" : 1,
            "IndicadorJuros" : false,
            "QuantidadeCalculo" : 1,
            "IndicadorFormaPagamentoSelecionado" : false,
            "ValorParcela" : 93.72,
            "Descricao" : "A VISTA FB",
            "DiaPreferencialPagamento" : 10,
            "ValorFracionado" : 0,
            "ValorIof" : 6.44,
            "CodigoFormaPagamento" : "0050"
        }, 
        {
            "Tipo" : "FB",
            "NumeroParcelas" : 2,
            "IndicadorJuros" : false,
            "QuantidadeCalculo" : 2,
            "IndicadorFormaPagamentoSelecionado" : false,
            "ValorParcela" : 46.86,
            "Descricao" : "1 + 1 FB",
            "DiaPreferencialPagamento" : 10,
            "ValorFracionado" : 0,
            "ValorIof" : 6.44,
            "CodigoFormaPagamento" : "0050"
        }, 
        {
            "Tipo" : "FB",
            "NumeroParcelas" : 3,
            "IndicadorJuros" : false,
            "QuantidadeCalculo" : 3,
            "IndicadorFormaPagamentoSelecionado" : false,
            "ValorParcela" : 31.24,
            "Descricao" : "1 + 2 FB",
            "DiaPreferencialPagamento" : 10,
            "ValorFracionado" : 0,
            "ValorIof" : 6.44,
            "CodigoFormaPagamento" : "0050"
        }, 
        {
            "Tipo" : "FB",
            "NumeroParcelas" : 4,
            "IndicadorJuros" : false,
            "QuantidadeCalculo" : 4,
            "IndicadorFormaPagamentoSelecionado" : false,
            "ValorParcela" : 23.43,
            "Descricao" : "1 + 3 FB",
            "DiaPreferencialPagamento" : 10,
            "ValorFracionado" : 0,
            "ValorIof" : 6.44,
            "CodigoFormaPagamento" : "0050"
        }, 
        {
            "Tipo" : "DC",
            "NumeroParcelas" : 1,
            "IndicadorJuros" : false,
            "QuantidadeCalculo" : 1,
            "IndicadorFormaPagamentoSelecionado" : true,
            "ValorParcela" : 93.72,
            "Descricao" : "A VISTA (DC)",
            "DiaPreferencialPagamento" : 10,
            "ValorFracionado" : 0,
            "ValorIof" : 6.44,
            "CodigoFormaPagamento" : "0071"
        }, 
        {
            "Tipo" : "DC",
            "NumeroParcelas" : 2,
            "IndicadorJuros" : false,
            "QuantidadeCalculo" : 2,
            "IndicadorFormaPagamentoSelecionado" : false,
            "ValorParcela" : 46.86,
            "Descricao" : "1+1 (DC)",
            "DiaPreferencialPagamento" : 10,
            "ValorFracionado" : 0,
            "ValorIof" : 6.44,
            "CodigoFormaPagamento" : "0071"
        }, 
        {
            "Tipo" : "DC",
            "NumeroParcelas" : 3,
            "IndicadorJuros" : false,
            "QuantidadeCalculo" : 3,
            "IndicadorFormaPagamentoSelecionado" : false,
            "ValorParcela" : 31.24,
            "Descricao" : "1+2 (DC)",
            "DiaPreferencialPagamento" : 10,
            "ValorFracionado" : 0,
            "ValorIof" : 6.44,
            "CodigoFormaPagamento" : "0071"
        }, 
        {
            "Tipo" : "DC",
            "NumeroParcelas" : 4,
            "IndicadorJuros" : false,
            "QuantidadeCalculo" : 4,
            "IndicadorFormaPagamentoSelecionado" : false,
            "ValorParcela" : 23.43,
            "Descricao" : "1+3 (DC)",
            "DiaPreferencialPagamento" : 10,
            "ValorFracionado" : 0,
            "ValorIof" : 6.44,
            "CodigoFormaPagamento" : "0071"
        }, 
        {
            "Tipo" : "CC",
            "NumeroParcelas" : 1,
            "IndicadorJuros" : false,
            "QuantidadeCalculo" : 1,
            "IndicadorFormaPagamentoSelecionado" : false,
            "ValorParcela" : 93.72,
            "Descricao" : "A VISTA (CC)",
            "DiaPreferencialPagamento" : 10,
            "ValorFracionado" : 0,
            "ValorIof" : 6.44,
            "CodigoFormaPagamento" : "0072"
        }, 
        {
            "Tipo" : "CC",
            "NumeroParcelas" : 2,
            "IndicadorJuros" : false,
            "QuantidadeCalculo" : 2,
            "IndicadorFormaPagamentoSelecionado" : false,
            "ValorParcela" : 46.86,
            "Descricao" : "1+1 (CC)",
            "DiaPreferencialPagamento" : 10,
            "ValorFracionado" : 0,
            "ValorIof" : 6.44,
            "CodigoFormaPagamento" : "0072"
        }, 
        {
            "Tipo" : "CC",
            "NumeroParcelas" : 3,
            "IndicadorJuros" : false,
            "QuantidadeCalculo" : 3,
            "IndicadorFormaPagamentoSelecionado" : false,
            "ValorParcela" : 31.24,
            "Descricao" : "1+2 (CC)",
            "DiaPreferencialPagamento" : 10,
            "ValorFracionado" : 0,
            "ValorIof" : 6.44,
            "CodigoFormaPagamento" : "0072"
        }, 
        {
            "Tipo" : "CC",
            "NumeroParcelas" : 4,
            "IndicadorJuros" : false,
            "QuantidadeCalculo" : 4,
            "IndicadorFormaPagamentoSelecionado" : false,
            "ValorParcela" : 23.43,
            "Descricao" : "1+3 (CC)",
            "DiaPreferencialPagamento" : 10,
            "ValorFracionado" : 0,
            "ValorIof" : 6.44,
            "CodigoFormaPagamento" : "0072"
        }, 
        {
            "Tipo" : "DC",
            "NumeroParcelas" : 2,
            "IndicadorJuros" : false,
            "QuantidadeCalculo" : 2,
            "IndicadorFormaPagamentoSelecionado" : false,
            "ValorParcela" : 46.86,
            "Descricao" : "1+1 (DC - PARCELA 1 FB)",
            "DiaPreferencialPagamento" : 10,
            "ValorFracionado" : 0,
            "ValorIof" : 6.44,
            "CodigoFormaPagamento" : "0171"
        }, 
        {
            "Tipo" : "DC",
            "NumeroParcelas" : 3,
            "IndicadorJuros" : false,
            "QuantidadeCalculo" : 3,
            "IndicadorFormaPagamentoSelecionado" : false,
            "ValorParcela" : 31.24,
            "Descricao" : "1+2 (DC - PARCELA 1 FB)",
            "DiaPreferencialPagamento" : 10,
            "ValorFracionado" : 0,
            "ValorIof" : 6.44,
            "CodigoFormaPagamento" : "0171"
        }, 
        {
            "Tipo" : "DC",
            "NumeroParcelas" : 4,
            "IndicadorJuros" : false,
            "QuantidadeCalculo" : 4,
            "IndicadorFormaPagamentoSelecionado" : false,
            "ValorParcela" : 23.43,
            "Descricao" : "1+3 (DC - PARCELA 1 FB)",
            "DiaPreferencialPagamento" : 10,
            "ValorFracionado" : 0,
            "ValorIof" : 6.44,
            "CodigoFormaPagamento" : "0171"
        }, 
        {
            "Tipo" : "CC",
            "NumeroParcelas" : 2,
            "IndicadorJuros" : false,
            "QuantidadeCalculo" : 2,
            "IndicadorFormaPagamentoSelecionado" : false,
            "ValorParcela" : 46.86,
            "Descricao" : "1+1 (CC - PARCELA 1 FB)",
            "DiaPreferencialPagamento" : 10,
            "ValorFracionado" : 0,
            "ValorIof" : 6.44,
            "CodigoFormaPagamento" : "0172"
        }, 
        {
            "Tipo" : "CC",
            "NumeroParcelas" : 3,
            "IndicadorJuros" : false,
            "QuantidadeCalculo" : 3,
            "IndicadorFormaPagamentoSelecionado" : false,
            "ValorParcela" : 31.24,
            "Descricao" : "1+2 (CC - PARCELA 1 FB)",
            "DiaPreferencialPagamento" : 10,
            "ValorFracionado" : 0,
            "ValorIof" : 6.44,
            "CodigoFormaPagamento" : "0172"
        }, 
        {
            "Tipo" : "CC",
            "NumeroParcelas" : 4,
            "IndicadorJuros" : false,
            "QuantidadeCalculo" : 4,
            "IndicadorFormaPagamentoSelecionado" : false,
            "ValorParcela" : 23.43,
            "Descricao" : "1+3 (CC - PARCELA 1 FB)",
            "DiaPreferencialPagamento" : 10,
            "ValorFracionado" : 0,
            "ValorIof" : 6.44,
            "CodigoFormaPagamento" : "0172"
        }
    ],
    "Cenarios" : [ 
        {
            "IndicadorCenarioEscolhido" : true,
            "IndicadorDescontoSeguroAuto" : false,
            "TipoAssistencia" : 0,
            "ValorPremioLiquido" : 87.28,
            "ValorCustoAdicionalApolice" : 0,
            "ValorDescontoPromocional" : null,
            "ValorCustoApolice" : 0,
            "PercentualReconquista" : null,
            "PercentualProLaboreEstipulante" : 0,
            "PercentualComissao" : 5,
            "PercentualCoeficienteAgravo" : null,
            "PercentualProLaboreSubEstipulante" : 0,
            "QuantidadeCalculo" : 0,
            "PercentualIOF" : 6.44,
            "ValorAdicionalFracionamento" : 0,
            "Itens" : [ 
                {
                    "CodigoCanal" : 6,
                    "CodigoProduto" : 11030,
                    "ValorLimiteMaximoGarantia" : null,
                    "Renovacao" : {
                        "CodigoSusepSeguradora" : null,
                        "FatorLimitador" : 0,
                        "FatorOtimizado" : 0,
                        "NomeSeguradora" : "",
                        "NumeroApolice" : "",
                        "NumeroItemApolice" : 0,
                        "PercentualDescontoPromocao" : 0,
                        "PeriodoCoberturaAnterior" : null,
                        "SinistroPremio" : false,
                        "ValorDesconto" : null,
                        "ValorDescontoFidelidade" : null,
                        "ValorDescontoPromocionalInformadoFilial" : 0
                    },
                    "EnderecoRisco" : {
                        "NomeBairro" : "PACAEMBU",
                        "NomeCidade" : "SAO PAULO",
                        "NomeLogradouro" : "WENDELL WILKIE",
                        "NumeroCep" : 1236050,
                        "NumeroLogradouro" : "84",
                        "TipoLogradouro" : "PRACA",
                        "SiglaUf" : "SP",
                        "ComplementoEndereco" : "",
                        "IndicadorImovelDesocupadoReformaConstrucao" : false,
                        "IndicadorImovelTombadoPatrimonioHistorico" : false,
                        "IndicadorMadeiraParedeExterna" : false,
                        "IndicadorRuralUrbano" : null,
                        "IndicadorSeguradoProprietario" : true,
                        "CodigoTipoConstrucao" : 4,
                        "DescricaoTipoConstrucao" : "ALVENARIA - SUPERIOR SÓLIDA",
                        "CodigoTipoHomeOffice" : 9,
                        "DescricaoTipoHomeOffice" : "NÃO UTILIZA PARA FINS COMERCIAIS",
                        "CodigoTipoResidencia" : 1,
                        "DescricaoTipoResidencia" : "CASA",
                        "CodigoTipoUtilizacao" : 1,
                        "DescricaoTipoUtilizacao" : "HABITUAL"
                    },
                    "DescontoProtecaoRoubo" : {
                        "IndicadorAlarme" : false,
                        "IndicadorCercaEletrica" : false,
                        "IndicadorGradesProtecao" : false,
                        "PercentualDesconto" : null
                    },
                    "Coberturas" : [ 
                        {
                            "Codigo" : 11429,
                            "Nome" : "INCÊNDIO/ EXPLOSÃO DE QUALQUER NATUREZA/ FUMAÇA E QUEDA DE AERONAVES",
                            "TipoCoberturaIncendio" : "E10",
                            "ValorLMI" : 10000,
                            "ParticipacaoObrigatoria" : null,
                            "CodigoVerba" : "E10",
                            "NomeVerba" : "PREDIO/CONTEUDO",
                            "CodigoFranquia" : "1"
                        }, 
                        {
                            "Codigo" : 11409,
                            "Nome" : "RESPONSABILIDADE CIVIL FAMILIAR",
                            "TipoCoberturaIncendio" : null,
                            "ValorLMI" : 150,
                            "ParticipacaoObrigatoria" : "VERBA UNICA",
                            "CodigoVerba" : "R04",
                            "NomeVerba" : "VERBA UNICA",
                            "CodigoFranquia" : null
                        }
                    ]
                }
            ]
        }
    ],
    "Filial" : {
        "Codigo" : 16,
        "Nome" : "BATEL"
    },
    "StatusCotacao" : [ 
        {
            "Codigo" : 1,
            "Descricao" : "Em cotação",
            "DataInclusao" : ISODate("2019-11-21T12:09:12.289Z"),
            "Usuario" : null
        }, 
        {
            "Codigo" : 1,
            "Descricao" : "Em cotação",
            "DataInclusao" : ISODate("2019-11-21T12:09:17.681Z"),
            "Usuario" : "naoAutenticado"
        }, 
        {
            "Codigo" : 1,
            "Descricao" : "Em cotação",
            "DataInclusao" : ISODate("2019-11-21T12:23:43.956Z"),
            "Usuario" : null
        }, 
        {
            "Codigo" : 1,
            "Descricao" : "Em cotação",
            "DataInclusao" : ISODate("2019-11-21T12:23:48.855Z"),
            "Usuario" : "naoAutenticado"
        }, 
        {
            "Codigo" : 1,
            "Descricao" : "Em cotação",
            "DataInclusao" : ISODate("2019-11-21T13:25:27.760Z"),
            "Usuario" : null
        }, 
        {
            "Codigo" : 1,
            "Descricao" : "Em cotação",
            "DataInclusao" : ISODate("2019-11-21T13:25:31.701Z"),
            "Usuario" : "naoAutenticado"
        }, 
        {
            "Codigo" : 1,
            "Descricao" : "Em cotação",
            "DataInclusao" : ISODate("2019-11-22T13:45:20.599Z"),
            "Usuario" : null
        }, 
        {
            "Codigo" : 1,
            "Descricao" : "Em cotação",
            "DataInclusao" : ISODate("2019-11-22T13:45:24.764Z"),
            "Usuario" : "naoAutenticado"
        }
    ],
    "DataLimiteCalculoEnvioProposta" : null,
    "EmailKitVirtual" : "#NULL#",
    "IndicadorKitVirtual" : null,
    "NomeOperador" : "#NULL#"
}